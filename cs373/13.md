# CS 373 Spring 2024: Benjamin Zimmerman

```
blog 13
2024-04-21
```

---

![A portrait of me.](../img/me4.jpg)

### What did you do this past week?

I mostly worked on projects, rotating between the last phase of the IDB project, the Life simulation project, and a personal project. I've also had some family stuff going on outside of class.

### What's in your way?

Still mostly just time. The end of the semester is as hectic as usual, and as I mentioned earlier, a fair bit of my time and attention has gone to family matters recently. Juggling everything has been tough, but the light at the end of the tunnel is visible.

### What will you do next week?

I will finish and submit my projects, give a presentation, finish the last paper and blog post, then start getting ready to head back home after the final meetings of the semester. I'm excited to be back home.

### What did you think of Paper \#13, "What Happens to Us Does Not Happen to Most of You"?

It's disappointing and infuriating that stories like these are so seemingly ubiquitous among women, both in our field as well as many others.

### What did you think of Refactoring and the ethics lectures?

I really like the idea of formalizing common refactoring patterns and establishing best practices. I get the impression that there's still some material to explore here, but with the semester wrapping up, I'm afraid I'll have to look into getting and reading Fowler's book.

I enjoyed the ethics lectures. These questions are hard, but they carry incredibly heavy consequences, so finding answers (or, at least, good approximations of answers) is of dire importance.

### What made you happy this week?

The NBA playoffs have begun, and although I haven't been able to watch many games yet, I'm excited to head back home and watch the end of the season with friends. *(I'm also quite happy that the Los Angeles Lakers have lost both of their games so far.)*

### What's your tip-of-the-week?

Trying new things in this industry is invaluable. You don't need to commit to using them, but at least give it a shot on something like a small personal project. Note the differences to the things you're used to working with. Look for things you like and things you don't like. Take note of clever, convenient, or otherwise "nice" things that you might not have gotten with your usual tools. If you're looking for a suggestion: Try making something with Rust or Go; Explore using protocol buffers for data serialization instead of JSON; Try managing a project using Mercurial or SVN instead of Git.
