# CS 373 Spring 2024: Benjamin Zimmerman

```
blog 03
2024-02-04
```

---

![A portrait of me.](../img/me4.jpg)

### What did you do this past week?

Aside from the usual classwork which is starting to ramp up, I had some urgent family matters come up. I'm also starting to work on a personal project with some friends which has been pretty fun thus far.

### What's in your way?

The aforementioned urgent family matters delayed some things, but I was still able to get everything done. I also still don't have a working campus VPN, but with Docker, that hasn't really caused any problems thus far.

### What will you do next week?

Aside from the projects for OOP and SWE, I'm starting to feel a little cooped up. I'd like to find some time to maybe go to a new restaurant with friends or maybe play some pickup basketball.

### What did you think of Paper #3, Continuous Integration?

I thought it was a great introduction to a practice I've seen frequently but I've never really learned about. It was also very interesting to see how things have changed in the 18 years since the article was published - there was an entire paragraph discussing options for version control software that didn't mention Git, which was about a year old at the time and probably hadn't yet gained the popularity it has today. *I saw a mention of [Subversion](https://subversion.apache.org/) and a shiver went down my spine.*

### What did you think of exceptions?

I thought I was already familiar with how exceptions worked, but I found it incredibly useful to go through some specific variances in how they work. I learned a lot that I didn't know before.

### What made you happy this week?

It was pretty satisfying to run the tests and see a wall of green "PASSED" pop up. *(Admittedly this week was a bit of a rough one.)* My friend also sent me a picture of his cat Sumo to help cheer me up:

![A picture of Sumo.](../img/sumo2.jpg)

### What's your pick-of-the-week or tip-of-the-week?

Markdown is great. I've started using it for my blogs since it's familiar to me and easy to use, but it's designed to be great for taking notes with as well. With a good markdown editor like Obsidian, I've found that it's an incredibly useful tool for learning, both in the classroom environment as well as in my own personal life.
