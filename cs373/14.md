# CS 373 Spring 2024: Final Entry: Benjamin Zimmerman

```txt
blog 14
2024-04-28
```

---

![A portrait of me.](../img/me4.jpg)

## How well do you think this course conveyed its takeaways?

Overall, I think this course did well in conveying its takeaways. There are some things that I think might be worth mentioning:

- Although the course did well to emphasize the importance of testing in general, I think frontend testing in particular can be confusing to many. I think some more guidance on that topic could help a lot - for example, a cursory example of separating API code from rendering code, or an example of how to use mock data in a testing suite.
- The takeaway about collaboration is important, though it requires a positive experience with collaborating to fully internalize. However, since most classes tend to rely more on noncollaborative work and since computer science students tend to be at least a little socially awkward, there's a real chance not all students will get that positive experience. That's okay. The important thing is to stay open-minded and to try and make each collaboration better than the last. Notice the things that do well and learn from them; similarly, notice the things that don't do well and think about what you can do to improve it next time.

## Were there any other particular takeaways for you?

As we learned new technologies, I found it interesting to find opiniated articles or videos on the tool and compare each to some of its popular alternatives. I think this is a great practice to efficiently become more familiar with a technology, which in turn helps you write better, more idiomatic code.

## What required tool did you not know and now find very useful?

React is the first reactive rendering engine I've used. I'd previously found it a bit daunting, and although I know there's a whole world of more "advanced" features of React that we didn't explore for this class, I'm glad to have now learned the basics.

## What's the most helpful Web dev tool your group used that was not required?

Vite was an incredibly useful frontend tool for rapidly iterating on code as well as building for deployment.

## How did you feel about your group having to self-teach many technologies?

I originally got into computer science through web dev, so I was fortunate to already be familiar with a decent amount of the self-taught technologies and standards. I mention that because although I found the self-taught portion of this class to be reasonable, I don't think my experience would be the same as someone coming into this class without that prior knowledge.

## In the end, how much did you learn relative to other CS classes?

I learned a lot, and I think this class is more practical and applicable than many other CS classes.

## Other improvements

- **CATME team formation** and **Civic engagement requirement on project**: I understand and appreciate the reasoning behind each of these, but I don't think they currently work well together. In particular, it's unlikely an assigned group of five students share a passion for the same cause. I think overall quality of projects would increase if the group assignment process took shared interests into account.
- **Pecha Kucha on YouTube**: I have a mild speech impediment, so I really appreciated that that we could record and edit the presentation.
- **Weekly papers on Perusall**: The expectations and requirements for annotation felt nebulous. I often had trouble figuring out what to annotate because I didn't really understand why I was annotating. Eventually I just settled on empmasizing points I felt were understated by the article, but I never really felt like the annotations significantly aided in my understanding.
- **TA mentoring on Slack**: Daniel was incredibly helpful every time our group needed help.
