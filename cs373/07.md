# CS 373 Spring 2024: Benjamin Zimmerman

```
blog 07
2024-03-03
```

---

![A portrait of me.](../img/me4.jpg)

### What did you do this past week?

I (finally!) installed Ubuntu on my laptop, made some decent progress on projects, and forgot my phone on a bus. Luckily I was able to get it back quickly though!

### What's in your way?

This week has been pretty stressful for me due to things outside of class. I'm thankful that my school-related workload for this week was somewhat flexible, but I'll definitely be doing some catch up work this next week as a result.

### What will you do next week?

My biggest tasks will be finishing and submitting Allocator project for 371p as well as Phase 2 of the IDB project for 373. Once I'm done with this week's work, I'm looking forward to heading home for spring break.

### What did you think of Paper \#7, "The Liskov Substitution Principle"?

In the past I've struggled a lot with confusing some of the SOLID / OOP principles with each other. At first, my understanding of the LSP was that it's basically just applied polymorphism and abstraction. However, the example with squared and rectangles in particular really helped me understand the problem that LSP is intended to avoid and how it's distinct from some of the other concepts we've discussed before. I also gained a lot of insight from the "real-world example" with third-party containers - this example really helped me gain a more practical understanding of SOLID thinking.

### What did you think of comprehensions, closures, and decorators?

Comprehensions seem pretty intuitive to me, though understanding the different types of comprehensions can be a little tricky. They almost feel like syntactic sugar. Closures and decorators both seem like incredibly powerful mechanisms, and I think it will take some more practice to gain a better understanding of their nuances and how/when to best use them.

### What made you happy this week?

The Spurs got two wins this week: One against the Oklahoma City Thunder and the other against the Indiana Pacers. In particular, the win against the Thunder was pretty big in the context of the [Rookie of the Year race](https://www.nba.com/news/kia-rookie-ladder-feb-28-2024-edition).

### What's your tip of the week?

This is a repeat tip, but it has once again proven important in my life, so I think it's worth repeating: Have backup plans! In the software development world, there are some obvious manifestations of this, such as file backups and version control. However, I would encourage you to go further with this advice: What are the potential points of failure in your workflow, how big of an impact they might have, and how you can plan for them in advance? For example, what would happen if your main computer suddenly stopped working? Could you use the UTCS lab computers, or are they insufficient for what you need? *(Keep in mind that since you don't have `sudo` perms, you might not have permission to install everything you need.)*

Of course, this tip applies to life in general, not just to software development. For example, when I lost my phone on the bus, there were three questions that quickly came to mind: Who has access to my phone's location, who can I quickly get in contact with without my phone, and *(most importantly)* who is in both of these groups? I realized that the only person in both of these groups is a close friend, and I got incredibly lucky that he happened to be available at that time. In order to be better prepared for situations like this in the future, I've made sure I have multiple forms of contact for my family members and my close friends, I've committed some of their phone numbers to memory, and I've decided to carry small list of contacts in my wallet as well.
